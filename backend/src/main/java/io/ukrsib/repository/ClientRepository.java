package io.ukrsib.repository;

import io.ukrsib.dto.Client;
import io.ukrsib.dto.Transaction;

import java.util.List;

public interface ClientRepository {

    boolean hasClient(Client client);

    List<Transaction> findClientTransaction(Client client);

    List<Client> getClients();

    void addTransaction(Client client, Transaction transaction);
}
