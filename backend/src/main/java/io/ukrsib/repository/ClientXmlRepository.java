package io.ukrsib.repository;

import io.ukrsib.dto.Client;
import io.ukrsib.dto.Transaction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class ClientXmlRepository implements ClientRepository {
    private final HashMap<Client, List<Transaction>> clients = new HashMap<>();

    @Override
    public boolean hasClient(Client client) {
        return clients.containsKey(client);
    }

    @Override
    public List<Transaction> findClientTransaction(Client client) {
        return clients.get(client);
    }

    @Override
    public List<Client> getClients() {
        return new ArrayList<>(clients.keySet());
    }

    @Override
    public void addTransaction(Client client, Transaction transaction) {
        List<Transaction> clientTransactions = findClientTransaction(client);

        if (clientTransactions == null) {
            clientTransactions = new ArrayList<>();
        }

        clientTransactions.add(transaction);
        clients.put(client, clientTransactions);
    }
}
