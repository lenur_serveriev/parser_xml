package io.ukrsib.dto;

public class Client {

    private Integer inn;

    private String firstName;

    private String lastName;

    private String middleName;

    public Client(Integer inn) {
        this.inn = inn;
    }

    public Client() {
    }

    public Integer getInn() {
        return inn;
    }

    public void setInn(Integer inn) {
        this.inn = inn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }

        if (!(object instanceof Client)) {
            return false;
        }

        Client client = (Client) object;

        return client.inn.equals(inn);
    }

    @Override
    public int hashCode() {
        int result = 17;

        return 31 * result + inn;
    }
}
