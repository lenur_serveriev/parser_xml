package io.ukrsib.controller;

import io.ukrsib.dto.Client;
import io.ukrsib.dto.Transaction;
import io.ukrsib.repository.ClientRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import java.util.List;

@RestController
public class ClientController {
    private final ClientRepository repository;

    public ClientController(ClientRepository repository) {
        this.repository = repository;
    }

    @CrossOrigin
    @RequestMapping(path = "/clients/{inn}/transactions", method = RequestMethod.GET)
    public List<Transaction> getTransactions(@PathVariable("inn") Integer inn) {
        Client client = new Client(inn);

        if (!repository.hasClient(client)) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }

        return repository.findClientTransaction(client);
    }

    @CrossOrigin
    @RequestMapping("/clients")
    public List<Client> home() {
        return repository.getClients();
    }
}
