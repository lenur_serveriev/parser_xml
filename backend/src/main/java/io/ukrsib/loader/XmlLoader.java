package io.ukrsib.loader;

import io.ukrsib.dto.Client;
import io.ukrsib.dto.Transaction;
import io.ukrsib.repository.ClientXmlRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;

@Component
public class XmlLoader implements ApplicationRunner {

    private final ClientXmlRepository repository;

    public XmlLoader(ClientXmlRepository repository) {
        this.repository = repository;
    }

    public void run(ApplicationArguments args) throws XMLStreamException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("test.xml");

        XMLInputFactory factory = XMLInputFactory.newInstance();

        XMLEventReader eventReader = factory.createXMLEventReader(inputStream);

        Client client = null;
        Transaction transaction = null;

        while (eventReader.hasNext()) {
            XMLEvent xmlEvent = eventReader.nextEvent();

            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();

                if ("transaction".equalsIgnoreCase(startElement.getName().getLocalPart())) {
                    client = new Client();
                    transaction = new Transaction();
                }

                switch (startElement.getName().getLocalPart()) {
                    case "place":
                        Characters place = (Characters) eventReader.nextEvent();
                        transaction.setPlace(place.getData());
                        break;
                    case "currency":
                        Characters currency = (Characters) eventReader.nextEvent();
                        transaction.setCurrency(currency.getData());
                        break;
                    case "card":
                        Characters card = (Characters) eventReader.nextEvent();
                        transaction.setCard(card.getData());
                        break;
                    case "amount":
                        Characters amount = (Characters) eventReader.nextEvent();
                        transaction.setAmount(Float.parseFloat(amount.getData()));
                        break;
                    case "firstName":
                        Characters firstName = (Characters) eventReader.nextEvent();
                        client.setFirstName(firstName.getData());
                        break;
                    case "lastName":
                        Characters lastName = (Characters) eventReader.nextEvent();
                        client.setLastName(lastName.getData());
                        break;
                    case "middleName":
                        Characters middleName = (Characters) eventReader.nextEvent();
                        client.setMiddleName(middleName.getData());
                        break;
                    case "inn":
                        Characters inn = (Characters) eventReader.nextEvent();
                        client.setInn(Integer.parseInt(inn.getData()));
                        break;
                }
            }

            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();

                if ("transaction".equalsIgnoreCase(endElement.getName().getLocalPart())) {
                    repository.addTransaction(client, transaction);
                }
            }
        }
    }
}
