import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Client} from "../model/client";
import {environment} from "../../environments/environment";
import {Transaction} from "../model/transaction";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {
  }

  getClients() {
    return this.http.get<Client[]>(environment.api + '/clients');
  }

  getTransaction(inn: number) {
    return this.http.get<Transaction[]>(environment.api + `/clients/${inn}/transactions`);
  }
}
