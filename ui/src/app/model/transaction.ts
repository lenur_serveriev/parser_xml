export class Transaction {

  amount: number;

  place: string;

  currency: string;

  card: string;
}
