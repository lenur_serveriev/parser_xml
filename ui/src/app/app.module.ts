import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ClientComponent} from "./component/client.component";

@NgModule({
  declarations: [
    ClientComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [ClientComponent]
})
export class AppModule {
}
