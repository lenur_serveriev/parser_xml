import {Component} from '@angular/core';
import {Client} from "../model/client";
import {ClientService} from "../service/client.service";
import {Transaction} from "../model/transaction";

@Component({
  selector: 'app-root',
  templateUrl: './client.component.html',
})

export class ClientComponent {
  title = 'Clients';
  clients: Client[] = [];
  selectedClient: Client;
  transactions: Transaction[] = [];

  constructor(private clientService: ClientService) {
    clientService
      .getClients()
      .subscribe(data => this.clients = data);
  }

  getTransaction(client: Client) {
    this.selectedClient = client;
    this.clientService
      .getTransaction(client.inn)
      .subscribe(data => this.transactions = data);
  }
}
