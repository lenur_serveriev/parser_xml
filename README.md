# Uksibbank client application

## Project stack:
- java 8 and spring framework in backend
- angular, bootstrap in frontend. Tested at the nodejs version 8 and npm 6.5

## Run backend
Move to the project source and and package project with command `mvn package`. 
After run jar with command `java -jar backend/target/ukrsib-backend-1.0.jar`. Navigate to `http://127.0.0.1:8383/clients` 
and see all clients parsed from xml source. For example the url `http://127.0.0.1:8383/clients/{inn}/transactions` return client transactions.

## Run frontend
Move to the frontend source: `ui/`. Install all libraries with command `npm install`. 
For the development you can use the command `ng serve`. Navigate to `http://localhost:4200/`. 
You can see client ui ![Alt text](clients.png?raw=true "Client ui") 
Click the button ![Alt text](click_clients.png?raw=true "Click button")  and will enable transactions 
ui ![Alt text](transactions.png?raw=true "Transaction ui")

## Some improvement:
- for more performance need to integrate pagination